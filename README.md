# Hyperledger FAbric Network Config Generator 

This is a simple Nodejs program that helps with the generation and manipulation of a Hyperledger Fabric Network Config.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

A YAML library is required for this tool to work. To install it, run npm install from the root directory:

```
cd hyperledger-fabric-network-config-mainpulator

npm install
```

### Installing

To install, simply clone this repository and "require" the two source files in the /src/ folder.

1. Clone the repository:
```
git clone https://gitlab.com/Jackyeoh/hyperledger-fabric-network-config-manipulator.git
```

2. "require" the source files
```
const ConfigClasses = require('./hyperledger-fabric-network-config-manipulator/src/configClasses.js');

const hfnc-manipulator = require('./hyperledger-fabric-network-config-manipulator/src/hfnc-manipulator.js');
```

## Using the tool

1. Create Empty / Load a Network Config

This tool maintain a network config internally. To start use, always start by creating an empty network-config by:

```
hfnc.generateEmptyNetworkConfig(name, xType, description, version);
```
or load one from file system:
```
hfnc.load('./path/to/existing-network-config.yaml');    // accepted file extensions are .yaml or .json
```

2. Manipulate Network Config

After a network config is created/loaded, begin manipulating it by using one of the following commands:

###Orderer

An orderer cannot be removed when its the only orderer in any one channel.

```
addOrderer('orderer.example.com', new ConfigClasses.OrdererConfig(
    'grpcs://localhost:7050',
    new ConfigClasses.GRPCOptions('orderer.example.com'),
    new ConfigClasses.CryptoOptions('pem', "-----BEGIN CERTIFICATE----- <etc>")
));
removeOrderer('orderer.example.com');
```

###Channel

To create a channel, an array of valid orderers must be specified.
All orgs, peers, and orderes in a channel will not be removed when removing a channel as they might be part of other channel.

```
addChannel('mychanne', new ConfigClasses.ChannelConfig(["orderer.example.com"], ['testcc:v0']));
removeChannel('mychannel');
```

###Chaincode

All chaincodes must be added to a specific channel.

```
addChaincode('mychannel', 'mycc', 'v0');
removeChaincode('mychannel', 'mycc', 'v0');
```

###Certificate Authorities (CA)

A CA cannot be removed when it is the only CA used by a organization.

```
addCA(new ConfigClasses.CAConfig(
        'ca-Org1',
        'https://localhost:7054', 
        new ConfigClasses.HTTPOptions(true), 
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../Admin@org1.example.com-cert.pem'),
        new ConfigClasses.RegistrarOptions("admin", "adminpw")
    ));
removeCA('ca-Org1');
```

###Organizations

To create an organization, the list of CAs specified must first be added into the network.

Additionally, when an organization is removed, all peers belong to said organization will be automatically removed as well.

```
addOrg(new ConfigClasses.OrgConfig(
        'Org1', 
        'Org1MSP', 
        ['ca-Org1'], 
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../Admin@org1.example.com-cert.pem'),
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../signcert.pem')
    ));
removeOrg('Org1');
```

###Peers

A peer must belong to an existing organization.

```
addPeer(new ConfigClasses.PeerConfig(
        'peer0',
        'Org1', 
        'mychannel', 
        new ConfigClasses.PeerAccessRights(true, true, true, true), 
        'grpcs://localhost:7051', 
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../Admin@org1.example.com-cert.pem'),
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../signcert.pem')
    ));
removePeer('peer0', 'Org1');
```

3. Save to file system 

To output the manipulated network config to the file system to be consumed by hyperledger fabirc, use:

```
hfnc.output('yaml', './path/to/output-network-config.yaml');    //// accepted file extensions are .yaml or .json
```


End with an example of getting some data out of the system or using it for a little demo

```
const hfnc = require('hfnc-manipulator');
const ConfigClasses = hfnc.ConfigClasses;

hfnc.generateEmptyNetworkConfig('my network config','hlf1v1','test empty network config','v1.0.0');

let orderer = new ConfigClasses.OrdererConfig(
    'grpcs://localhost:7050',
    new ConfigClasses.GRPCOptions('orderer.example.com'),
    new ConfigClasses.CryptoOptions('pem', "-----BEGIN CERTIFICATE----- <etc>")
);
hfnc.addOrderer('orderer.example.com', orderer);

let channel = new ConfigClasses.ChannelConfig(["orderer.example.com"], ['testcc:v0']);
hfnc.addChannel('mychannel', channel);

hfnc.addChaincode('mychannel', 'mycc', 'v0');
hfnc.addChaincode('mychannel', 'mycc', 'v1');

let ca = new ConfigClasses.CAConfig(
        'ca-Org1',
        'https://localhost:7054', 
        new ConfigClasses.HTTPOptions(true), 
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../Admin@org1.example.com-cert.pem'),
        new ConfigClasses.RegistrarOptions("admin", "adminpw")
    );
hfnc.addCA(ca);

let org = new ConfigClasses.OrgConfig(
        'Org1', 
        'Org1MSP', 
        ['ca-Org1'], 
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../Admin@org1.example.com-cert.pem'),
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../signcert.pem')
    );
hfnc.addOrg(org);

let peer = new ConfigClasses.PeerConfig(
        'peer0',
        'Org1', 
        'mychannel', 
        new ConfigClasses.PeerAccessRights(true, true, true, true), 
        'grpcs://localhost:7051', 
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../Admin@org1.example.com-cert.pem'),
        new ConfigClasses.CryptoOptions('path', './crypto-config/.../signcert.pem')
    );
hfnc.addPeer('mychannel', peer);

hfnc.output('yaml', './networkConfig.yaml');
```

## Running the tests

Common test cases are all included in the ""./test.js". To run the test, navigate to the root directory and run:
```
node test
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Jack Yeoh** - *Initial work* - [Jackyeoh](https://gitlab.com/Jackyeoh)

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details
