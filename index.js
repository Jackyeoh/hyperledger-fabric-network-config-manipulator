'use static';
const fs = require('fs');
const YAML = require('yaml');
const ConfigClasses = require('./configClasses.js');

//  Internal representation of the network config object
let networkConfig;

//  Internal representation of the network's naming Schemes
let scheme = {
    org:{
        prefix:'.',
        suffix:'.example.com'
    },
    chaincode:{
        seperator:':'
    }
};

//  ===== API =====
/**
 * Loads a network-config into memory in json form.
 * @param {string} pathToFile - Path pointing to the network-config file. The only file extension accepted is .yaml or .json.
 */
function load(pathToFile){
    try{
        let extension = extractFileExtensionFromPath(pathToFile);
        if (extension === "yaml"){
            networkConfig = YAML.parse(fs.readFileSync(pathToFile));
        }else if (exntension === "json"){
            networkConfig = JSON.parse(fs.readFileSync(pathToFile));
        }else{
            throw new TypeError('network-config with invalid extension detected.');
        }
        return networkConfig;
    }catch(err){
        if (err instanceof TypeError){
            //  Throw type error to caller to handle
            throw err;
        }
    }
}

/**
 * Generate a network config file in the file system that can be consumed by Hyperledger Fabric
 * 
 * @param {string} type         - the type of file to ouput. Currently only support .json and .yaml
 * @param {string} writePath    - Path to output the file.
 */
function output(type, writePath){
    //  Exported functions and ConfigClasses will appear in the networkConfig obj,
    //  Strip the functions off the object before stringify to yaml
    let strippedConfig = JSON.parse(JSON.stringify(networkConfig));
    delete strippedConfig.ConfigClasses;
    if (type === 'yaml'){
        fs.writeFileSync(writePath, YAML.stringify(strippedConfig));
    }else if (type === 'json'){
        fs.writeFileSync(writePath, JSON.stringify(strippedConfig, null, 2));
    }
}

/**
 * Generates an empty network config. 
 * 
 * This network config needs to be populated with orderer, channels, orgs, and peers before it can be
 * used in a network.
 * 
 * @param {string} name         - Name of this network config.
 * @param {string} xType        - only takes 'hlfv1'.
 * @param {string} description  - describe what this network cofig is for.
 * @param {string} version      - version of network config to generate.
 */
function generateEmptyNetworkConfig(name, xType, description, version){
    this.name = name;
    this['x-type'] = xType;
    this.description = description;
    this.version = version;
    this.client = null;
    this.channels = {};
    this.organizations = {};
    this.orderers = {};
    this.peers = {};
    this.certificateAuthorities = {};

    networkConfig = this;
    return networkConfig;
}



function addChannel(channelName, channelConfig){
    validateNetworkConfig();

    //  Check if orderers are present
    channelConfig.orderers.forEach((ordererName)=>{
        if (!networkConfig.orderers[ordererName]){
            throw new Error(`${ordererName} is not present in the network config. Please addOrderer() before trying again.`);
        }
    });

    //  Add channel to channels
    networkConfig.channels[channelName] = channelConfig;
}

function removeChannel(channelName){
    validateNetworkConfig();

    //  Remove channel from channels
    delete networkConfig.channels[channelName];
}

function addPeer(peerConfig){
    validateNetworkConfig();
    //  Check if channel is present
    if(!networkConfig.channels[peerConfig.channel]){
        throw new Error(`${peerConfig.channel} is not present in the network config. Please addChannel() before trying again.`);
    }
    //  Check if org is present
    if(!networkConfig.organizations[peerConfig.orgName]){
        throw new Error(`${peerConfig.orgName} is not present in the network config. Please addOrg() before trying again.`);
    }

    //  Split peerConfig to be included in different sections
    let peerFullName = getFullPeerName(peerConfig.peer, peerConfig.orgName);
    let accessRights = peerConfig.peerAccessRights;
    let otherConfigs = {
        url: peerConfig.url,
        grpcOptions: peerConfig.grpcOptions,
        tlsCACerts: peerConfig.tlsCACerts
    };

    //  Add peer to channel
    networkConfig.channels[peerConfig.channel].peers[peerFullName] = accessRights;

    //  Add peer to org
    networkConfig.organizations[peerConfig.orgName].peers.push(peerFullName);

    //  Add peer to list (update its config if already present)
    networkConfig.peers[peerFullName] = otherConfigs;
}

function removePeer(peerToRemove, sourceOrg){
    validateNetworkConfig();
    let peerFullName = getFullPeerName(peerToRemove, sourceOrg);
    //  Remove this peer from all channels
    for (let channel in networkConfig.channels){
        delete networkConfig.channels[channel].peers[peerFullName];
    }
    
    //  Remove this peer from its org
    let foundIndex = -1;
    networkConfig.organizations[sourceOrg].peers.forEach((peer, index)=>{
        if (peer === peerFullName){
            foundIndex = index;
        }
    });
    if(foundIndex != -1){
        networkConfig.organizations[sourceOrg].peers.pop(foundIndex);
    }else{
        throw new Error('potentially corrouped network config detected.');
    }

    //  Remove this peer from peer list
    delete networkConfig.peers[peerFullName];
}

function addChaincode(channel, chaincodeName, version){
    validateNetworkConfig();

    //  Check if channel exists
    if (!networkConfig.channels[channel]){
        throw new Error(`${channel} not found in network config.`);
    }

    //  Aggregate chaincode fullname
    let chaincodeFull = getFullChaincodeName(chaincodeName, version);

    //  Check if chaincode already exists
    let index = networkConfig.channels[channel].chaincodes.indexOf(chaincodeFull);
    if (index !== -1){
        throw new Error(`chaincode already exists`);
    }

    //  Add chaincode to channel
    networkConfig.channels[channel].chaincodes.push(chaincodeFull);
}

function removeChaincode(channel, chaincodeName, version){
    validateNetworkConfig();

    //  Check if channel exists
    if (!networkConfig.channels[channel]){
        throw new Error(`${channel} not found in network config.`);
    }

    //  Aggregate chaincode fullname
    let chaincodeFull = getFullChaincodeName(chaincodeName, version);

    //  Remove chaincode from channel
    networkConfig.channels[channel].chaincodes.push(chaincodeFull);
}

function addOrderer(ordererName, ordererConfig){
    validateNetworkConfig();

    //  Add orderer to orderers
    networkConfig.orderers[ordererName] = ordererConfig;
}

function removeOrderer(ordererToRemove){
    validateNetworkConfig();

    //  Check if this orderer is the only orderer in any channels
    let channelsInvolved = [];
    for(let channel in networkConfig.channels){
        let index = networkConfig.channels[channel].orderers.indexOf(ordererToRemove);
        if (index !== -1){
            channelsInvolved.push({channel: channel,index:index});
            if (networkConfig.channels[channel].orderers.length === 1){
                throw new Error(`cannot remove ${ordererToRemove} because it is the only orderer used in ${channel}`);
            }
        }
    }

    //  Remove from all channels that uses it
    channelsInvolved.forEach((entry)=>{
        networkConfig.channels[entry.channel].orderers.pop(entry.index);
    });

    //  Remove from orderers
    delete networkConfig.orderers[ordererToRemove];
}

function addOrg(orgConfig){
    validateNetworkConfig();
    //  Make sure all CAs is present
    orgConfig.certificateAuthorities.forEach((ca)=>{
        let caList = Object.keys(networkConfig.certificateAuthorities);
        if (!caList.includes(ca)){
            throw new Error(`${ca} is not present in the network config. Please addCA() before trying again.`);
        }
    });

    //  Extract config
    let config = {
        mspid                   :  orgConfig.mspid,
        peers                   :  orgConfig.peers,
        certificateAuthorities  :  orgConfig.certificateAuthorities,
        adminPrivateKey         :  orgConfig.adminPrivateKey,
        signedCert              :  orgConfig.signedCert
    };

    //  Add org to organizations
    networkConfig.organizations[orgConfig.key] = config;
}

function removeOrg(orgToRemove){
    validateNetworkConfig();

    //  Remove all peers that belong to this org
    let peers = networkConfig.organizations[orgToRemove].peers;
    peers.forEach((peerFullName)=>{
        let peer = getPeerFromPeerFullName(peerFullName, orgToRemove);
        removePeer(orgToRemove, peer);
    });

    //  Remove org from organizations
    delete networkConfig.organizations[orgToRemove];
}

function addCA(caConfig){
    validateNetworkConfig();

    //  Seperate key and config
    let key = caConfig.caName;
    let config = {
        url         : caConfig.url,
        httpOptions : caConfig.httpOptions,
        tlsCACerts  : caConfig.tlsCACerts,
        registrar   : caConfig.registrar,
        caName      : caConfig.caName
    };

    //  Add CA to certificateAuthorities
    networkConfig.certificateAuthorities[key] = config;
}

function removeCA(caToRemove){
    validateNetworkConfig();

    //  Check if this CA is the only CA used by any org
    let orgsUsingThisCA = [];
    for (let org in networkConfig.organizations){
        //  Check if the ca to be removed are used by this org
        let index = networkConfig.organizations[org].certificateAuthorities.indexOf(caToRemove);
        if (index !== -1){
            orgsUsingThisCA.push({org:org,index:index});
            if (networkConfig.organizations[org].certificateAuthorities.length === 1){
                //  Then the CA to be removed is the only CA left used by this org
                throw new Error(`cannot remove ${caToRemove} because it is the only CA used in ${org}`);
            }
        }
    }

    //  Remove from all orgs using it
    orgsUsingThisCA.forEach((entry)=>{
        networkConfig.organizations[entry.org].certificateAuthorities.pop(entry.index);
    });

    //  Remove from certificate authorities
    delete networkConfig.certificateAuthorities[caToRemove];
}

function setScheme(newScheme){
    if (!newScheme){
        throw new Error('cannot set scheme to null');
    }
    if (!newScheme.org){
        throw new Error(`scheme's org section is missing`);
    }
    if (!newScheme.org.prefix){
        throw new Error(`scheme's org.prefix is missing`);
    }
    if (!newScheme.org.suffix){
        throw new Error(`scheme's org.suffix is missing`);
    }
    if (!newScheme.chaincode){
        throw new Error(`scheme's chaincode section is missing`);
    }
    if (!newScheme.chaincode.seperator){
        throw new Error(`scheme's chaincode.seperator is missing`);
    }
    scheme = newScheme;
}

//  ===== Schema Related =====
function getFullPeerName(peer, org){
    if (!peer){
        throw new ReferenceError('peer cannot be null');
    }
    if (!org){
        throw new ReferenceError('org cannot be null');
    }
    return `${peer}${scheme.org.prefix}${org.toLowerCase()}${scheme.org.suffix}`;
}

function getPeerFromPeerFullName(peerFullName, org){
    if (!peerFullName){
        throw new ReferenceError('peerFullName cannot be null');
    }
    if (!org){
        throw new ReferenceError('org cannot be null');
    }
    let noOrg = peerFullName.split(org.toLowerCase())[0];
    let cutOffIndex = noOrg.lastIndexOf(scheme.org.prefix);
    return noOrg.substring(0, cutOffIndex);
}

function getFullChaincodeName(chaincodeName, version){
    return `${chaincodeName}${scheme.chaincode.seperator}${version}`;
}

//  ===== Utils =====
function validateNetworkConfig(){
    if (!networkConfig){
        throw new ReferenceError('Network Config not initialized. Try loading a network-config from file system or generating a new one.');
    }
}

function extractFileExtensionFromPath(path){
    let slices = path.split('.');
    return slices[slices.length - 1];
} 

exports.ConfigClasses = ConfigClasses;
exports.generateEmptyNetworkConfig = generateEmptyNetworkConfig;
exports.load = load;
exports.output = output;
exports.setScheme = setScheme;
exports.addOrderer = addOrderer;
exports.removeOrderer = removeOrderer;
exports.addChannel = addChannel;
exports.removeChannel = removeChannel;
exports.addChaincode = addChaincode;
exports.removeChaincode = removeChaincode;
exports.addCA = addCA;
exports.removeCA = removeCA;
exports.addOrg = addOrg;
exports.removeOrg = removeOrg;
exports.addPeer = addPeer;
exports.removePeer = removePeer;
exports.getFullPeerName = getFullPeerName;