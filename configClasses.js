function ChannelConfig(orderers, chaincodes){
    this.orderers = orderers;
    this.peers = {};
    this.chaincodes = chaincodes;
}

function OrgConfig(orgName, mspid, certificateAuthorities, adminPrivateKey, signedCert){
    this.key = orgName;
    this.mspid = mspid;
    this.peers = [];
    this.certificateAuthorities = certificateAuthorities;
    this.adminPrivateKey = adminPrivateKey;
    this.signedCert = signedCert;
}

function OrdererConfig(url, grpcOptions, tlsCACerts){
    this.url = url;
    this.grpcOptions = grpcOptions;
    this.tlsCACerts = tlsCACerts;
}

function PeerConfig(peer, orgName, channel, peerAccessRights, url, grpcOptions, tlsCACerts){
    this.peer = peer;
    this.orgName = orgName;
    this.channel = channel;
    this.peerAccessRights = peerAccessRights;
    this.url = url;
    this.grpcOptions = grpcOptions;
    this.tlsCACerts = tlsCACerts;
}

function CAConfig(caName, url, httpOptions, tlsCACerts, registrar){
    this.caName = caName;
    this.url = url;
    this.httpOptions = httpOptions;
    this.tlsCACerts = tlsCACerts;
    this.registrar = registrar;
}

function NetworkConfig(name, xType, description, version, channels, organizations, orderers, peers, certificateAuthorities){
    this.name = name;
    this['x-type'] = xType;
    this.description = description;
    this.version = version;
    this.client = null;
    this.channels = arrayToMap(channels);
    this.organizations = arrayToMap(organizations);
    this.orderers = arrayToMap(orderers);
    this.peers = arrayToMap(peers);
    this.certificateAuthorities = arrayToMap(certificateAuthorities);
}

//  ========== Options ==========
function CryptoOptions(type, crypto){
    this[type] = crypto;
}

function PeerAccessRights(endorsingPeer, chaincodeQuery, ledgerQuery, eventSource){
    if (typeof endorsingPeer !== "boolean"){
        throw TypeError('endorsingPeer has to be boolean');
    }
    if (typeof chaincodeQuery !== "boolean"){
        throw TypeError('chaincodeQuery has to be boolean');
    }
    if (typeof ledgerQuery !== "boolean"){
        throw TypeError('ledgerQuery has to be boolean');
    }
    if (typeof eventSource !== "boolean"){
        throw TypeError('eventSource has to be boolean');
    }
    this.endorsingPeer = endorsingPeer;
    this.chaincodeQuery = chaincodeQuery;
    this.ledgerQuery = ledgerQuery;
    this.eventSource = eventSource;
}

function HTTPOptions(verify){
    this.verify = verify;
}

function RegistrarOptions(enrollId, enrollSecret){
    this.enrollId = enrollId;
    this.enrollSecret = enrollSecret;
}

function GRPCOptions(sslTargetNameOverride){
    this["ssl-target-name-override"] = sslTargetNameOverride;
}

//  ========== Utils ==========
function arrayToMap(array){
    return array.reduce((map, obj)=>{
        let key = obj.key;
        delete obj.key;
        map[key] = obj;
        return map;
    },{});
}

exports.ChannelConfig = ChannelConfig;
exports.OrgConfig = OrgConfig;
exports.OrdererConfig = OrdererConfig;
exports.PeerConfig = PeerConfig;
exports.CAConfig = CAConfig;
exports.NetworkConfig = NetworkConfig;
exports.CryptoOptions = CryptoOptions;
exports.PeerAccessRights = PeerAccessRights;
exports.HTTPOptions = HTTPOptions;
exports.RegistrarOptions = RegistrarOptions;
exports.GRPCOptions = GRPCOptions;